Privacy Policy

Last updated [Sep 19th, 2020]
Our Privacy Policy forms part of and must be read in conjunction with, website Terms and Conditions. We reserve the right to change this Privacy Policy at any time.
We respect the privacy of our users and every person who visits our sites https://pointup.com.my/. Here,https://pointup.com.my/ (“we”, “us”, or “our”). We are committed to protecting your personal information and your right to privacy in accordance with the Personal Data Protection Act (PDPA). If you have any questions or concerns about our policy or our practices with regards to your personal information, please contact us at https://pointup.com.my/.
When you visit our website https://pointup.com.my/ and use our services, you trust us with your personal information. We take your privacy very seriously. In this privacy notice, we describe our privacy policy. We seek to explain to you in the clearest way possible what information we collect, how we use it, and what rights you have in relation to it. We hope you take some time to read through it carefully, as it is important. If there are any terms in this privacy policy that you do not agree with, please discontinue the use of our site and our services.
This privacy policy applies to all information collected through our website, and/or any related services, sales, marketing, or events (we refer to them collectively in this privacy policy as the “Site“).  
About us
At https://pointup.com.my/, we offer a meticulously designed website where we offer our partners to come on board and offer points, vouchers, coupons to their customer base, enabling merchants for “virtual member card”.
Our office is in Penang, Malaysia.

Please read this privacy policy carefully as it will help you make informed decisions about sharing your personal information with us.  
1. What information do we collect?
The personal information you disclose to us
We collect personal information that you voluntarily provide to us when expressing an interest in obtaining information about us or our products and services when participating in activities on the Site or otherwise contacting us.
2. How do we use your information?
We use your personal information for these purposes in reliance on our legitimate business interests (“Business Purposes”), in order to enter into or perform a contract with you (“Contractual”), with your consent (“Consent”), and/or for compliance with our legal obligations (“Legal Reasons”).
We use the information we collect or receive:  
    • To send administrative information to you
    • Deliver targeted advertising to you 
    • Request Feedback
    • To enable user-to-user communications
    • To enforce our terms, conditions, and policies
    • To respond to legal requests and prevent harm
    • For other Business Purposes
3. Will your information be shared with anyone?
We only share and disclose your information in the following situations:
    • Compliance with Laws. We may disclose your information where we are legally required to do so in order to comply with applicable law, governmental requests, a judicial proceeding, court order, or legal process, such as in response to a court order.
    • Vital Interests and Legal Rights. We may disclose your information where we believe it is necessary to investigate, prevent, or take action regarding potential violations of our policies, suspected fraud, situations involving potential threats to the safety of any person and illegal activities, or as evidence in litigation in which we are involved.
    • Vendors, Consultants, and Other Third-Party Service Providers. We may share your data with third-party vendors, service providers, contractors, or agents who perform services for us or on our behalf and require access to such information to do that work.
    • Affiliates. We may share your information with our affiliates, in which case we will require those affiliates to honor this privacy policy. Affiliates include our parent company and any subsidiaries, joint venture partners, or other companies that we control or that are under common control with us.
    • Business Partners. We may share your information with our business partners to offer you certain products, services, or promotions.
    • With your Consent. We may disclose your personal information for any other purpose with your consent.

4. Do we use cookies and other tracking technologies?
We may use cookies and similar tracking technologies (like web beacons and pixels) to access or store information. Specific information about how we use such technologies and how you can refuse certain cookies is set out in our Cookie Policy.
5. Is your information transferred internationally?
We will not transfer your personal information to an overseas recipient.
6. What is our stance for third party websites?
The Site may contain advertisements from third parties that are not affiliated with us and which may link to other websites, online services, or mobile applications. We cannot guarantee the safety and privacy of data you provide to any third parties. Any data collected by third parties is not covered by this privacy policy. We are not responsible for the content or privacy and security practices and policies of any third parties, including other websites, services, or applications that may be linked to or from the Site. You should review the policies of such third parties and contact them directly to respond to your questions.
7. How long do we keep your information?
We will only keep your personal information for as long as it is necessary for the purposes set out in this privacy policy unless a longer retention period is required or permitted by law (such as tax, accounting, or other legal requirements). No purpose in this policy will require us to keep your personal information for longer than 90 days past the termination of your account.
8. What are your privacy rights?
1.Personal Information
You may at any time review or change the information in your account or terminate your account by:
    • Contacting us using the contact information provided below
Some information may be retained in our files to prevent fraud, troubleshoot problems, assist with any investigations, enforce our Terms of Use, and/or comply with legal requirements.
Cookies and similar technologies: Most Web browsers are set to accept cookies by default. If you prefer, you can usually choose to set your browser to remove cookies and to reject cookies. If you choose to remove cookies or reject cookies, this could affect certain features or services of our Site.
9. PDPA Entitlement
We are committed to protecting your privacy in accordance with the Personal Data Protection Act 2010 of Malaysia (“PDPA”). This Policy explains:  the type of personal data we collect and how we collect it  how we use your personal data  the parties that we disclose the personal
10. Do we make updates to this policy?
We may update this privacy policy from time to time. The updated version will be indicated by an updated “Revised” date and the updated version will be effective as soon as it is accessible.
11. DATA PROTECTION OFFICER
We have appointed a Data Protection Officer (“DPO”) who is responsible for overseeing questions in relation to this privacy notice. If you have any questions about this privacy notice, including any requests to exercise your legal rights, please contact the Data Protection Officer, at our email  feedback@pointup.com.my/
12. How can you contact us about this policy?
If you have questions or comments about this policy, email us at feedback@pointup.com.my/




